# Arrays/Excercise_1.rb

arr = [1, 3, 5, 7, 9, 11]
number = 3

arr.include?(number) ? (puts "#{number} is in the array") : (puts "#{number} is NOT in the array")



# Excercise 2

puts 'a)  The value of arr is [[b],[b, 2], [b,3], [a,1], [a,2], [a,3]]'
puts 'b) The value of the arr is [b], [a, [1,2,3]]'

#Excercise 3

arr = [["test", "hello", "world"],["example", "mem"]]
# return the word 'example'

arr.last[0]

#Excercise 4

arr = [15, 7, 18, 5, 12, 8, 5, 1]

arr.index(5)  #3
arr.index[5]  #error
arr[5]  #8

#Excercise 5

string = "Welcome to America!"
a = string[6] #e
b = string[11]  #A
c = string[19]  #nil

#Excercise 6

names = ['bob', 'joe', 'susan', 'margaret']
#names['margaret'] = 'jody'
names[3] = 'jody'

#Excercise 7
#program that iterates over an array and builds a new array that increments each value by 2.  
#print the original array and the new array

def incrementor(arr)
  arr.map { |e| e + 2 }
end

a = [1,2,3]
a_new = incrementor(a)

p a
p a_new


