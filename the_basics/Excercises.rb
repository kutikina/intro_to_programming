# Excercise 1
puts "Excercise 1"

puts "Bob " + "Smith"

#Excercise 2 Displays integer values of places in a four numbered integer

puts "Excercise 2"
a = 4563
puts "The number is: #{a}."

puts "Thousands - #{a/1000}, Hundreds - #{a%1000/100}, Tens - #{a%1000%100/10}, Ones - #{a%1000%100%10}"

#Excercise 3 Uses a hash to store moves and years.

puts "Excercise 3"

movies = {:avengers => '1974', :roots => '1988', :gone_with_the_wind => '1968', :batman => '2001'}
puts "#{movies[:avengers]}"
puts "#{movies[:roots]}"
puts "#{movies[:gone_with_the_wind]}"
puts "#{movies[:batman]}"

#or
puts "or using hash.each"
movies.each {|key, value| puts "#{value}"}


#Excercise 4 using an array to store the movie years
puts "Excercise 4"

movies = [1974, 1988, 1968, 2001]
puts "#{movies[0]}"
puts "#{movies[1]}"
puts "#{movies[2]}"
puts "#{movies[3]}"

#Excercise 5 The factorial of 5, 6, 7 and 8
puts "Excercise 5"

puts "The factorial of 5 is #{5*4*3*2*1}"
puts "The factorial of 6 is #{6*5*4*3*2*1}"
puts "The factorial of 7 is #{7*6*5*4*3*2*1}"
puts "The factorial of 8 is #{8*7*6*5*4*3*2*1}"

#Excercise 6 Squares of three float numbers
puts "Excercise 6"

puts "The square of 93.7 is #{93.7*93.7}"
puts "The square of 58.3446 is #{58.3446*58.3446}"
puts "The square of 359.89 is #{359.89*359.89}"

#Excercise 7 Error message interpretation
puts "Excercise 7"

puts "This error message: SyntaxError: (irb):2: syntax error, unexpected ')', expecting '}'
  from /usr/local/rvm/rubies/ruby-2.0.0-rc2/bin/irb:16:in `<main>'"
puts "Means that a ')' has been typed instead of a '}' on line 2 of the irb session"
