# Defines a method that takes two numbers and returns the product

def multiply(a,b)
  a*b
end

puts "Please enter the first number..."
first = gets.chomp.to_i
puts "Please enter the second number..."
second = gets.chomp.to_i
puts "The product is: " + multiply(first, second).to_s
