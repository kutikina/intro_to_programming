def scream(words)
  words = words + "!!!!"
  return
  puts words
end

scream("Yippeee")
# return value is 'nil' as nothing was written after 'return' 

#alternative
def scream2(words)
  words = words + "!!!!"
  puts words
end

scream("Yippeee")
#return is 'nil'
