#numbers.rb

#takes a number between 0 and 100 and reports if between 0 and 50, 51 and 100 or above 100

def get_number
  puts "Enter a number between 0 and 100..."
  n = gets.chomp.to_i
  test_number(n)
end

def test_number(number)
  if number < 0
    puts "Try again, the number is less than 0."
    get_number
  elsif number > 100
    puts "Try again, the number is greater than 100."
    get_number
  elsif number <= 50 
    puts "The number is between 0 and 50"
  else
    puts "The number is between 50 and 100"
  end
end

get_number

puts "And now using a case statment"

def get_number
  puts "Enter a number between 0 and 100..."
  n = gets.chomp.to_i
  test_number(n)
end

def test_number(number)
  case
  when number < 0
    puts "Try again, the number is less than 0."
    get_number
  when number > 100
    puts "Try again, the number is greater than 100."
    get_number
  when number <= 50
    puts "The number is between 0 and 50"
  else
    puts number
    puts "The number is between 50 and 100"
  end
end

get_number

