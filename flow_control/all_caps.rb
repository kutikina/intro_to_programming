#all_Caps.rb
#takes a string and returns an all caps version only if string is longer than 10 characters

def caps(string)
  string.upcase if string.length > 10 || string
end

puts "Enter a string..."
a = gets.chomp

puts caps(a)