#family_members.rb

#Given a hash with keys and an array for the values, use select to choose immediate
#family members to new array

family = {  uncles: ["bob", "joe", "steve"],
            sisters: ["jane", "jill", "beth"],
            brothers: ["frank","rob","david"],
            aunts: ["mary","sally","susan"]
          }


direct_family = family.select {|k, v| k == :sisters || k == :brothers}

new_family = direct_family.values.flatten
p new_family



