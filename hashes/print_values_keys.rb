#print_values_keys.rb

h = {horse: 'jed', cat: 'tinky', dog: 'monty'}

h.each_key {|key| puts key}
h.each_value {|value| puts value}
h.each_pair {|key, value| puts "key: #{key} value: #{value}"}