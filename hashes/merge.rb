#merge.rb

#illustrate the difference between merge and merge!

puts "First using /'merge/'"
h1 = {colour: 'red', shape: 'round', fruit: 'apple'}
h2 = {fruit: 'mango', price: 3.50, discount: 0.20}

h1.merge(h2)
p h1

puts "Now using /'merge!/'"
h1 = {colour: 'red', shape: 'round', fruit: 'apple'}
h2 = {fruit: 'mango', price: 3.50, discount: 0.20}

h1.merge(h2)
p h1


