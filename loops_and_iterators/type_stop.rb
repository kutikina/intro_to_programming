#type_stop.rb

#Asks for input and does somethign till the user types STOP

def get_input
  puts "Please enter a first name or STOP to exit"
  gets.chomp
end

input = get_input

while input != "STOP" do
  puts "The name is " + input.upcase + " ."
  input = get_input
end
