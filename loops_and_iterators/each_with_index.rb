#each_with_index.rb

#uses the each_with_index method to iterate through an array and pring both number and index

a = ['bob', 'david', 'peter', 'jim']

a.each_with_index do |name, index| 
  puts "#{index + 1}. #{name}."
end