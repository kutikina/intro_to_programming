#Inputs a first and last name, outputs the whole name in a string

puts "What is your first name?"
first_name = gets.chomp
puts "Great, got that #{first_name}, what is your last name?"
last_name = gets.chomp
puts "So your full name is #{first_name} #{last_name}."
