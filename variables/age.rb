#asks for an age then outputs the age in 10, 20, 30 and 40 years.

puts "How old are you?"
age = gets.chomp.to_i
puts "Ok, so now you are #{age} years old."
puts "In 10 years you will be #{age + 10} years old."
puts "In 20 years you will be #{age + 20} years old."
puts "In 30 years you will be #{age + 30} years old."
puts "In 40 years you will be #{age + 40} years old."
